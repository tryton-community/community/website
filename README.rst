=========================
Tryton Community Website
=========================

Uses `Nikola Static Site Generator <https://getnikola.com/>`_.

Building the Website::

  python3 -m venv _venv --system-site-packages
  source _venv/bin/activate
  python -m pip install -r requirements.txt
  nikola auto
