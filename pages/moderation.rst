.. title: Moderation and Moderator Team

“Tryton Community” and the repositories
will be administered by a team of moderators.
The team will be elected “somehow” (how, is still to be discussed),
the period of office is expected to be no longer than two years.

Initial maintainers are
`Hartmut Goebel <https://foss.heptapod.net/htgoebel>`_ and
`Sergi Almacellas Abellana <https://foss.heptapod.net/pokoli>`_.


Moderator's responsibilities, rights and obligations
======================================================

Maintainers foster the aims of *Tryton Community*.
They are *not* expected to maintain any of the repositories.
Maintainers should have a good idea of what’s going on,
but the other responsibilities can (and should! :-)) be delegated.

Their duties are:

* Enforcing *Tryton Community* to be a warm and welcoming place:
  maintainers are the contact point for anyone who wants to report abuse.

* Enforcing the :doc:`“Tryton Community” policies <policy>`.

* Making decisions, about code or anything,
  if consensus cannot be reached.

* Technically owning and managing the
  `"Tryton Community" group at Heptapod
  <https://foss.heptapod.net/tryton-community>`_.

* Decide about accepting new projects for “Community” and
  creating a respective sub-project in Heptapod.

* Maintainers are allowed to add or remove access to/from sub-projects.
  This is especially true for projects
  that are obviously unmaintained.
