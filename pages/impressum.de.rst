.. title: Impressum


Betreiber dieser Website und verantwortlich für den Inhalt
----------------------------------------------------------

| Goebel Consult
| Dipl.-Informatiker (univ.) Hartmut Goebel
| Salamanderweg 5 · 84034 Landshut · Deutschland
| Fon: 0871 / 66 06 318
| E-Mail: h.goebel@crazy-compilers.con
