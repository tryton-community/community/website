.. title: Unser Service für die Community

Um die Arbeit der Mitwirkenden zu vereinfachen,
plant die *Tryton Community* zentrale Entwicklungsdienste anbieten,
wie zum Beispiel:

* ein `Code-Repository <https://foss.heptapod.net/tryton-community>`_,
  mit Issue Tracking und Merge Requests, und

* *Continuous Integration* (CI) für automatisierten Tests,

* einen Übersetzungsdienst,

* Projektvorlagen
  (z.B. mit `Cookiecutter <(https://www.cookiecutter.io/>`_), etc.

* Label werden von CI automatisch gesetzt,
  abhängig von den Ergebnissen der Tests

* Ein Skript zum Paketieren und Veröffentlichen von Modulen
  wird im Repository eingebunden.
  Details müssen noch festgelegt werden.
  Wir überlegen  beispielsweise,
  eine einfache Möglichkeit zu bieten,
  ein Dockerimage mit individuell gewählten Community-Modulen zu erstellen.


.. note::

   Ob die hier vorgestellten Ideen tatsächlich umgesetzt werden,
   hängt davon ab, wie stark sich Mitwirkenden einbringen.
   Die von *Tryton Community* angebotenen Dienste
   sollen sich im Laufe der Zeit weiterentwickeln.


.. Emacs config:
 Local Variables:
 mode: rst
 ispell-local-dictionary: "german"
 End:
