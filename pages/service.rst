.. title: Our Service for the Community

To make it easy to contribute,
*Tryton Community* plans to provide central development services like:

* a `Code-Repository <https://foss.heptapod.net/tryton-community>`_,
  with issue tracking und merge requests, and

* *Continuous Integration* (CI) for automated tests,

* some translation service,

* project templates
  (z.B. using `Cookiecutter <https://www.cookiecutter.io/>`_), etc.

* Label will be set automatically by the CI,
  depending on the result of the tests.

* A script for packaging and publishing modules
  will be included in the repository.
  Details still need to be determined.
  For example, we are considering
  providing a simple way to create
  a Docker image with individually chosen Community modules.


.. note::

   Whether the ideas scratched here will be implemented actually
   depends on how active contributors are.
   The services provided by *Tryton Community*
   are planed to evolve over time.
