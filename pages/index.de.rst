.. title: Tryton Community

Die *Tryton Community* unterstützt die Arbeit auf
`tryton.org <https://tryton.org>`_,
um die Fähigkeiten des ERP-Systems Tryton zu erweitern.
*Tryton Community* ist herzlich und einladend,
offen für alle Beiträge zu Tryton.
Wer sich an :doc:`wenige grundlegende Regeln <policy>` hält,
findet hier einen Sammelpunkt für Beiträge,
die auf tryton.org keinen Platz finden, insbesondere

* ergänzende Module

* Dokumentation und Videos,

* Werkzeuge, zum Beispiel zur Installation,
  Administration und Dokumentation,

* Kernmodule, die um zusätzliche Fähigkeiten ergänzt wurden.

Die *Tryton Community* will die Zusammenarbeit
an Tryton-Modulen und -Werkzeugen attraktiv machen,
insbesondere um Doppel- und Mehrfacharbeit zu vermeiden.
Die *Tryton Community* soll ein Ort sein,
an dem eine lebendige und agile Tryton-Gemeinschaft wachsen kann.

Die *Tryton Community* plant,
:doc:`zentrale Entwicklungsdienste <service>` bereitstellen,
um Mitwirkende bei der Entwicklung hochwertiger Projekte zu unterstützen.


Ein Raum für freie Entwicklung
===================================

*Tryton Community* schafft einen Raum,
in der sich die Weiterentwicklung von Tryton freier entfalten kann.
*Tryton Community* möchte Tryton insbesondere auch Anwendern
ohne Programmierkenntnisse besser zugänglich machen.
Daher laden wir alle im Projekt Tryton Aktiven ein,
Beiträge zu *Tryton Community* zu liefern.
Dazu gibt es beispielsweise folgende Möglichkeiten:

* lade ein Modul `auf das Tryton-Community-Repository
  <https://foss.heptapod.net/tryton-community/modules>`_.

* lade uns Anwender- oder Programmier-Dokumentation
  `auf das Repo - egal in welcher Sprache
  <https://foss.heptapod.net/tryton-community/documentation>`_.

* schreibe uns eine Nachricht und weisen Sie uns auf
  Ihr Projekt hin,

* verbessere das Design dieser Website,

* übersetze
  diese `Website <https://foss.heptapod.net/tryton-community/community/website>`_
  oder das
  `Tryton Buch <https://foss.heptapod.net/tryton-community/documentation/das-tryton-buch>`_,

* helfe, CI/CD auf Heptapod zu implementieren,

* …

.. hint::

   Wenn Du ein Projekt einbringen möchtest,
   `erstelle bitte einen Issue im tryton-community @ heptapod project`__
   mit der Vorlage "Hosting Request".

__ https://foss.heptapod.net/tryton-community/community/heptapod/-/issues/new


Eine Sammelstelle für Ideen
===================================

Es gibt eine Vielzahl von Modulen und anderen Komponenten für Tryton,
die wenig bekannt sind.
Damit wird erschreckend viel Potential für eine effiziente Nutzung
von Tryton verschenkt.
*Tryton Community* versteht sich als Sammelstelle
all dieser Ideen und Konzepte, um

* Doppel- und Mehrfacharbeit zu vermeiden,

* eine Quelle für Inspiration und Ideen zu bieten,

* den fachlichen Austausch zu fördern, sowie

* die Effizienz bei der Entwicklung für und der
  Nutzung von Tryton zu steigern.


.. Emacs config:
 Local Variables:
 mode: rst
 ispell-local-dictionary: "german"
 End:
