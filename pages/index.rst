.. title: Tryton Community

*Tryton Community* supports the work on
`tryton.org <https://tryton.org>`_
to extend the capabilities of the Tryton ERP system.
*Tryton Community* is warm and welcoming,
open to all contributions to Tryton.
If you follow a :doc:`few basic rules <policy>`,
this is a gathering place for contributions
that have no place on tryton.org, especially

* complementary modules,

* documentation and videos,

* tools, for example for installation, administration and documentation,

* core modules that have been extended with additional capabilities.

*Tryton Community* aims to make collaboration
on Tryton modules and tools attractive,
especially to avoid duplication and multiple efforts.
The *Tryton Community* is intended to be a place
where a vibrant and agile Tryton community can grow.

The community plans to provide
:doc:`centralized development services <service>`
to help contributors develop high-quality projects.


A Space for Free Development
===================================

*Tryton Community* creates a space
where Tryton development can unfold more freely.
*Tryton Community* wants to make Tryton more accessible
especially to users without programming skills.
Therefore, we invite all those active in the Tryton project
to contribute to *Tryton Community*.
You can do this in the following ways, for example:

* upload a module to the
  `“Tryton Community” repository
  <https://foss.heptapod.net/tryton-community/modules>`_.

* upload user or programming documentation
  `to the “Tryton Community” repo - no matter in which language
  <https://foss.heptapod.net/tryton-community/documentation>`_

* write us a message and point us to your project,

* improve the design of this website,

* translate
  this `website <https://foss.heptapod.net/tryton-community/community/website>`_
  or the
  `Tryton Book <https://foss.heptapod.net/tryton-community/documentation/das-tryton-buch//>`_,

* help implementing the CI/CD at Heptapod,

* …

.. hint::

   If you want to bring in your project,
   please `create an issue in tryton-community @ heptapod project`__
   with the "Hosting Request" template.

__ https://foss.heptapod.net/tryton-community/community/heptapod/-/issues/new


A Collection Point for Ideas
===================================

There are a lot of modules and other components for Tryton
that are little known.
This means that a lot of potential for an efficient use of Tryton is wasted.
*Tryton Community* sees itself as a collection point
for all these ideas and concepts in order to


* avoid duplication and multiplication of work

* to provide a source of inspiration and ideas

* promote professional exchange, and

* increase efficiency in the development and use of Tryton.
