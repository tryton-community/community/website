.. title: Impressum


Betreiber dieser Website und verantwortlich für den Inhalt
----------------------------------------------------------

| Dipl.-Informatiker (univ.) Hartmut Goebel
| Salamanderweg 5 · 84034 Landshut · Germany
| Fon: +49 871 6606318
| E-Mail: h.goebel@crazy-compilers.con
