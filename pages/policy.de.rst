.. title: Das Regelwerk

Dieser Abschnitt beschreibt die Richtlinien für die *Tryton Community*,
die wesentlich lockerer sind als die Richtlinien von *tryton.org*.
Die *Community*\-Mitglieder und Mitwirkenden
werden über detailliertere Regeln entscheiden,
wenn sich das Projekt weiterentwickelt.

Um das *Tryton Community* für Viele attraktiv zu machen,
gelten nur wenige Regeln:


Regeln
================

Benutzerdokumentation
-----------------------

*Tryton Community* legt besonderen Wert auf Anwenderfreundlichkeit,
auch für Menschen ohne Programmierkenntnisse.
Die Idee *code is documentation* genügt uns nicht.
Jedes Projekt muss daher eine `README`-Datei enthalten,
in der beschrieben ist:

  * was tut das Modul/Programm/Skript,
    für welchen Zweck wurde es verfasst,

  * wie wird es installiert,
    was ist dabei zu beachten,

  * wie wird es konfiguriert,
    welche Eingaben müssen wo vorgenommen werden,

  * wie wird es bedient, welche Schritte sind notwendig, um es zu nutzen.

Wenn Du Unterstützung brauchst, um diese Anforderungen zu erfüllen,
helfen wir gerne.

Ein Projekt, das noch in Arbeit ist und aufgenommen werden möchte,
muss in der `README`-Datei
zumindest eine kurzen Beschreibung seines Zwecks enthalten.
Ein einziger Satz reicht dafür in der Regel nicht aus.
Nach Fertigstellung muss die `README`-Datei geändert werden,
um den oben genannten Anforderungen zu entsprechen.


Finanzierung von Fehlerbehebungen und Weiterentwicklung
-------------------------------------------------------

*Tryton Community* möchte ein Ort sein,
wo von Anwender.innen gewünschte Funktionen,
die im Tryton Kern (noch) nicht realisiert wurden,
Wirklichkeit werden können.

Konzeption und Gestalt neuer Funktionen
sollten vorab im `Tryton-Forum <https://discuss.tryton.org>`_
diskutiert werden.

*Tryton Community* ist fest von FOSS überzeugt,
glaubt aber nicht an "frei wie Freibier".
(Weiter-) Entwicklungen kosten Geld,
weil auch Entwickler.innen ihre Rechnungen bezahlen müssen.

*Tryton Community* ermutigt daher Verfasser.innen
von Verbesserungsvorschlägen und Fehlerberichten,
auch einen monetären Beitrag für die Lösung des Problems anzubieten.
Dieser muss den zu erwartenden Aufwand *nicht vollständig* abdecken.
Vielmehr ist es erwünscht, daß sich andere *Community*-Mitglieder
an der Finanzierung dieser Projekte beteiligen
und in dem betreffenden issue ebenfalls einen Betrag anbieten.

*Tryton Community* wird vorläufig
die finanziellen Beiträge informell (per E-Mail) verwalten.
Wir arbeiten an einer einfacheren Lösung hierfür.


Lizenzen
--------

Projekte müssen unter einer Freien- oder Open-Source-Lizenz lizenziert sein.
Wir empfehlen, eine von
`AGPLv3-oder-neuer`, `GPLv3-oder-neuer`, `LGPLv3-oder-neuer` oder
`FDLv1.3-oder-neuer` zu verwenden.
Ohnehin wird die Lizenz `mit Tryton kompatibel
<https://www.gnu.org/licenses/gpl-faq.html#AllCompatibility>`__
sein müssen, das `GPLv3-or-later` verwendet.


Tests
------

Software-Projekte müssen innerhalb von 3 Monaten
nach Erstellung des Projekts eine minimale Test-Suite enthalten.
Für Module muss die Test-Suite zumindest die Standardtests der
``ModuleTestSuite`` enthalten.

  * Module, die alle Tests bestehen,
    werden mit "Tests bestanden" gekennzeichnet.

  * Module, die den Test nicht bestehen,
    werden mit "Tests nicht bestanden" gekennzeichnet.

  * Module, die keine Test-Suite enthalten,
    werden als "nicht testbar" gekennzeichnet und
    nach einiger Zeit in den „Dachboden“ verschoben.

Es wird empfohlen, die `Tryton-Codierungsrichtlinien
<https://www.tryton.org/develop/guidelines/code>`_ zu befolgen.


Tryton-Module
-------------

Für Module gilt:

  * Neue Projekte müssen mindestens die letzte Tryton-LTS-Version
    (Version X.0.\*) oder neuer unterstützen.

  * Für jede Hauptversion (Version X.Y.\*, wobei Y gerade ist),
    die das Modul unterstützt,
    wird ein zusätzlicher Branch erstellt.

  * Um die Stabilität zu gewährleisten,
    sind Änderungen an der Datenbankstruktur
    nur in Entwicklungsversionen erlaubt
    (Version X.Y.\*, wobei Y ungerade ist).
    Sobald eine Hauptversion veröffentlicht wird
    (Version X.Y.0, wobei Y gerade ist)
    muss die Datenbankstruktur
    für alle Veröffentlichungen dieser Hauptversion beibehalten werden.


Projektspezifische Regeln
--------------------------

Jedes Projekt in der *Tryton Community*
kann seine eigenen Prozesse und speziellen Regeln aufstellen,
solange diese nicht im Widerspruch zu den Regeln der *Tryton Community*
oder des Heptapod FOSS Hosting Service stehen.


Nicht-Regeln
================

* Da höchst unterschiedliche Auffassungen existieren,
  was "Code-Qualität" bedeutet,
  werden hierzu keine spezifischen Anforderungen formuliert.
  Wer mit der Qualität eines Projektes nicht zufrieden ist,
  ist eingeladen, diese zu verbessern.

* Es besteht keine Verpflichtung für die Betreuer,
  ihre Module auf neuere Hauptversionen zu migrieren.
  Module, die nicht migriert wurden,
  sind eben solange nicht für neue Hauptversionen verfügbar,
  bis sie jemand migriert.

* Module, die nicht auf eine der derzeit unterstützten Hauptversionen von
  Tryton migriert werden,
  werden als „ungewartet“ markiert und
  nach einiger Zeit in den „Dachboden“ verschoben.

* Projekte können selbst wählen,
  welches Versionskontrollsystem sie verwenden.
  Allerdings empfehlen wir Mercurial zu verwenden,
  insbesondere für Tryton-Module.


* Es steht den Projekten frei,
  mehrere zusammengehörige Module wie in einem kleinen Mono-Repo zu verwalten.



.. Emacs config:
 Local Variables:
 mode: rst
 ispell-local-dictionary: "german"
 End:
