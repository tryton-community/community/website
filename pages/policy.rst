.. title: Policy

This section describes the policy for *Tryton Community*,
which is much more relaxed the *tryton.org*'s policy.
*Tryton Community* members and contributors will decide
on a more detailed governance as the project evolves.

To make *Tryton Community* attractive for many, only a few rules apply:


Rules
================

User Documentation
--------------------

*Tryton Community* puts special emphasis on user-friendliness,
also for people without programming knowledge.
The idea *code is documentation* is not enough for us.
Therefore every project must contain a `README` file.
In it is described:

  * what does the module/program/script do,
    for what purpose was it written,

  * how it is installed,
    what is to be considered thereby,

  * how it is configured,
    which inputs have to be made where,

  * how it is operated, what steps are necessary to use it.

If you need support to meet these requirements,
we are happy to help.

A project which is work in progress and desires to be included
must in its `README` file
contain at least a brief description of its purpose.
A single sentence is usually not sufficient.
After completion,
the `README` file must be amended to match the requirements mentioned above.


Funding Bug-Fixes and Enhancements
-------------------------------------------

*Tryton Community* wants to be a place
where users can ask for functions
that have not (yet) been realized in Tryton Core,
can become reality.

The conception and design of new functions
should be discussed in advance
in the `Tryton Forum <https://discuss.tryton.org>`_.

*Tryton Community* is firmly convinced of FOSS,
but does not believe in "free as free beer".
(Further) developments cost money,
because developers have to pay their bills, too.

*Tryton Community* therefore encourages authors of
enhancement proposals and bug reports,
to offer a monetary contribution to the solution of the problem.
This does not have to cover *all* of the expected effort.
Rather, it is desirable that other *community* members
participate in the financing of these projects
and also offer an amount to the issue in question.

*Tryton Community* will provisionally
manage the financial contributions informally (by e-mail).
We are working on a simpler solution for managing this.


Licenses
-----------

Projects must be licensed under a free or open source license.
We recommend using one of `AGPLv3-or-later`, `GPLv3-or-later`,
`LGPLv3-or-later` or `FDLv1.3-or-later`.
Anyway the license will have to be `compatible with Tryton
<https://www.gnu.org/licenses/gpl-faq.html#AllCompatibility>`__,
which uses `GPLv3-or-later`.


Tests
--------

Software projects must provide a minimal test suite within 3 month
after creation of the project.
For modules the test suite must at least include the standard
``ModuleTestSuite``.

  * Modules that pass all tests,
    are labeled with "Tests passed".

  * Modules that do not pass the test,
    are labeled with "Tests failed".

  * Modules that do not contain a test suite,
    will be marked as "not testable" and
    moved to the “attic” after some time.

Following the `Tryton coding guidelines
<https://www.tryton.org/develop/guidelines/code>`_
is recommended.


Tryton Modules
---------------

For modules applies:

  * New projects must support at least the latest Tryton LTS version.
    (version X.0.\*) or newer.

  * For each major version (version X.Y.\*, Y being even)
    the module supports
    an additional branch will be created.

  * In order to ensure stability, changes to the database structure
    are only allowed in development versions
    (version X.Y.\*, Y being odd).
    Once a major version is released (version X.Y.0, Y being even)
    the database structure must be kept
    for all releases of this major version.


Project Specific Rules
------------------------

Each project in *Tryton Community*
can establish its own processes and special rules
as long as these are not contrary to the rules of *Tryton Community*,
or of the Heptapod FOSS hosting service.


Non-Rules
================

* Since highly divergent views exist,  what "code quality" means,
  no specific requirements are formulated for this.
  Anyone who is not satisfied with the quality of a project
  is invited to improve it,

* There is no obligation for the maintainers
  to migrate their modules to newer major versions.
  Modules that have not been migrated
  are just not available for new new major versions
  until someone migrates them.

* Modules not migrated to any of the currently supported Tryton major
  versions will be marked and ”unmaintained” and
  moved to the “attic” after some time.

* Projects can choose for themselves
  which version control system they use.
  However, we recommend to use Mercurial,
  especially for Tryton modules.

* Projects are free to manage several related modules
  in a small mono-repo.
