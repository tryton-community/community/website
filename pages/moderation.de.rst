.. title: Moderation, Moderatoren-Team

*Tryton Community* und die Repositories werden
technisch von einem Team von Moderatoren verwaltet.
Das Team wird "irgendwie" gewählt (wie, muss noch diskutiert werden),
die Amtszeit beträgt maximal zwei Jahre.

Die ersten Betreuer sind
`Hartmut Goebel <https://foss.heptapod.net/htgoebel>`_ und
`Sergi Almacellas Abellana <https://foss.heptapod.net/pokoli>`_.


Aufgaben, Rechte und Pflichten der Moderatoren
================================================

Moderatoren fördern die Ziele von *Tryton Community*.
Das Pflegen der Repositories ist *nicht* ihre Aufgabe.
Moderatoren müssen eine gute Vorstellung davon haben,
was vor sich geht,
die anderen Aufgaben können (und sollten :-)) delegiert werden.

Ihre Pflichten sind:

* Dafür sorgen, dass *Tryton Community*
  ein warmer und einladender Ort ist und bleibt.
  Die Betreuer sind die Anlaufstelle für jeden, der Verstöße melden möchte.

* Durchsetzung der :doc:`„Tryton-Community“-Richtlinien <policy>`.

* Entscheidungen über Code oder andere Dinge treffen,
  wenn kein Konsens erreicht werden kann.

* Technische Leitung und Verwaltung der
  `Gruppe „Tryton Community“ auf Heptapod
  <https://foss.heptapod.net/tryton-community>`_.

* Entscheidung über die Annahme neuer Projekte für *Tryton Community* und
  Erstellen eines Unterprojekts in Heptapod.

* Moderatoren können den Zugang zu/von Unterprojekten
  hinzufügen oder entfernen.
  Dies gilt insbesondere für Projekte,
  die offensichtlich nicht gewartet werden.


.. Emacs config:
 Local Variables:
 mode: rst
 ispell-local-dictionary: "german"
 End:
